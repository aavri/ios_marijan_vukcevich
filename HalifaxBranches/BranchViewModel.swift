//
//  BranchViewModel.swift
//  HalifaxBranches
//
//  Created by Marijan Vukcevich on 3/5/19.
//

import Foundation
import UIKit

struct BranchViewModel {
    
    let branchName: String
    let branchAddress: String
    let branchPhone: String
    
    init(branchModel: BranchInfo) {
        self.branchName = branchModel.branchName
        self.branchAddress = branchModel.branchAddressLine ?? ""
        self.branchPhone = branchModel.branchPhone ?? ""
    }
    
}
