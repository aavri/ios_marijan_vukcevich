//
//  BranchInfo.swift
//  HalifaxBranches
//
//  Created by Marijan Vukcevich on 3/5/19.
//

import Foundation

struct BranchInfo {
    
    let branchName: String
    let branchAddressLine: String?
    let branchTownName: String?
    let branchPostCode: String?
    let branchCountry: String?
    let branchPhone: String?
    
    init(branchName: String, branchAddressLine: String? = nil, branchTownName: String? = nil, branchPostCode: String? = nil, branchCountry: String? = nil, branchPhone: String? = nil) {
        self.branchName = branchName
        self.branchAddressLine = branchAddressLine
        self.branchTownName = branchTownName
        self.branchPostCode = branchPostCode
        self.branchCountry = branchCountry
        self.branchPhone = branchPhone
    }
}

