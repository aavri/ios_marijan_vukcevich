/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

struct Branch : Codable {
	let identification : String?
	let sequenceNumber : String?
	let name : String?
	let type : String?
	let customerSegment : [String]?
	let otherServiceAndFacility : [OtherServiceAndFacility]?
	let availability : Availability?
	let contactInfo : [ContactInfo]?
	let postalAddress : PostalAddress?

	enum CodingKeys: String, CodingKey {

		case identification = "Identification"
		case sequenceNumber = "SequenceNumber"
		case name = "Name"
		case type = "Type"
		case customerSegment = "CustomerSegment"
		case otherServiceAndFacility = "OtherServiceAndFacility"
		case availability = "Availability"
		case contactInfo = "ContactInfo"
		case postalAddress = "PostalAddress"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		identification = try values.decodeIfPresent(String.self, forKey: .identification)
		sequenceNumber = try values.decodeIfPresent(String.self, forKey: .sequenceNumber)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		customerSegment = try values.decodeIfPresent([String].self, forKey: .customerSegment)
		otherServiceAndFacility = try values.decodeIfPresent([OtherServiceAndFacility].self, forKey: .otherServiceAndFacility)
		availability = try values.decodeIfPresent(Availability.self, forKey: .availability)
		contactInfo = try values.decodeIfPresent([ContactInfo].self, forKey: .contactInfo)
		postalAddress = try values.decodeIfPresent(PostalAddress.self, forKey: .postalAddress)
	}

}
