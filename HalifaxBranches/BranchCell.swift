//
//  BranchCell.swift
//  HalifaxBranches
//
//  Created by Marijan Vukcevich on 3/5/19.
//

import Foundation
import UIKit

class BranchCell: UITableViewCell {

    let branchTitleLabel: UILabel = {
        let l = UILabel()
        l.numberOfLines = 0
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.init(rawValue: "13"))
        l.textAlignment = .left
        l.textColor = .red
        return l
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpCells()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setUpCells() {
        backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        addSubview(branchTitleLabel)
        branchTitleLabel.widthAnchor.constraint(equalToConstant: 220).isActive = true
        branchTitleLabel.heightAnchor.constraint(equalToConstant: 80).isActive = true
        branchTitleLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        branchTitleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
    }
    
    func displayBranchInCell(using viewModel: BranchViewModel) {
        branchTitleLabel.text = viewModel.branchName
    }
    
}
