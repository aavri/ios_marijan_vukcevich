//
//  BranchesTableViewController.swift
//  HalifaxBranches
//
//  Created by Marijan Vukcevich on 3/5/19.
//

import Foundation
import UIKit

class BranchesTableViewController: UITableViewController, UISearchBarDelegate {

    private let cellID = "CustomCell"
    private var branchesArray = [BranchInfo]()
    private var branchesContactsArray = [Branch]()
    private var resultTableData = [BranchInfo]()
    private var searchTownNameData = [String]()
    
    lazy var searchBar:UISearchBar = UISearchBar()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Branches"
        
        tableView.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        tableView.register(BranchCell.self, forCellReuseIdentifier: cellID)
        tableView.contentInset = UIEdgeInsets(top: 22, left: 0, bottom: 0, right: 0)
        
        
        searchBar.searchBarStyle = UISearchBar.Style.prominent
        searchBar.placeholder = " Search..."
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
        tableView.tableHeaderView = searchBar
 
        loadBranchInfo()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
     
    }
    
    func getJsonBranch(completionHandler: @escaping ([AnyObject]) -> ()) {
       
        let urlAPI = "https://api.halifax.co.uk/open-banking/v2.2/branches"
        
        guard let url = URL(string: urlAPI) else {
            return
        }
        
        let headers = [
            "if-modified-since": "REPLACE_THIS_VALUE",
            "if-none-match": "REPLACE_THIS_VALUE",
            "accept": "application/prs.openbanking.opendata.v2.2+json"
        ]
        
        let downloader = JSONHalifaxApi()
        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let task = downloader.jsonTask(with: request) { (result) in
            
                switch result {
                case .Error(let error):
                    print("error: ", error)
                    return
                case .Success(let json):
                   // completionHandler([json] as [JSONHalifaxApi.JSON] as [AnyObject])
                    completionHandler([json as AnyObject])
                }
        }
        task.resume()
    }
        
    func loadBranchInfo() {
        
           self.getJsonBranch { objArray in
            
            let n = objArray[0] as! HalifaxBrandMapping
            
            guard let mainData = n.data?[0], let brand = mainData.brand?[0], let branch = brand.branch else {
                return
            }
           
            self.branchesContactsArray = branch
        
                   DispatchQueue.main.async {
                    
                    for (_, value) in self.branchesContactsArray.enumerated() {
                       
                        let branchName = value.name ?? ""
                        let branchAddressLine = value.postalAddress?.addressLine?[0] ?? ""
                        let branchTownName = value.postalAddress?.townName ?? "Missing Town's Name"
                        self.searchTownNameData.append(branchTownName)
                        let branchPostCode = value.postalAddress?.postCode ?? ""
                        let branchCountry = value.postalAddress?.country ?? ""
                        let branchPhone = value.contactInfo?[0].contactContent ?? ""
                       
                        let branchInfo = BranchInfo(branchName: branchName, branchAddressLine: branchAddressLine, branchTownName: branchTownName, branchPostCode: branchPostCode, branchCountry: branchCountry, branchPhone: branchPhone)
                    
                       self.branchesArray.append(branchInfo)
                        
                    }
                    
                     self.resultTableData = self.branchesArray
                     self.tableView.reloadData()
                    
                    }
            }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        resultTableData.removeAll(keepingCapacity: false)
        
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchBar.text!.lowercased())
      
        if searchText.isEmpty {
            self.resultTableData = branchesArray
        } else {
            let array = branchesArray.filter { searchPredicate.evaluate(with: $0.branchTownName?.lowercased()) }
            self.resultTableData  = array
        }
        self.tableView.reloadData()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.resultTableData = branchesArray
        self.tableView.reloadData()
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.resultTableData.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID) as! BranchCell
    
        let branches =  resultTableData[indexPath.row] //branchesArray[indexPath.row]
        let branchViewModel = BranchViewModel(branchModel: branches)
        cell.displayBranchInCell(using: branchViewModel)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         tableView.deselectRow(at: indexPath, animated: true)
        
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "goToSecondVC") as UIViewController
        (viewController as! BranchDetailViewController).branchesArray = self.branchesArray
        (viewController as! BranchDetailViewController).index = indexPath.row
        let navigationController = UINavigationController(rootViewController: viewController)
        self.present(navigationController, animated: true, completion: nil)
    }

}
