//
//  JSONHalifaxApi.swift
//  HalifaxBranches
//
//  Created by Marijan Vukcevich on 3/5/19.
//

import Foundation

struct JSONHalifaxApi {
    
    let session: URLSession

    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    init() {
        self.init(configuration: .default)
    }
    
    typealias JSON = HalifaxBrandMapping //Dictionary<String, AnyObject> //[String: AnyObject]
    typealias JSONTaskCompletionHandler = (Result<JSON>) -> ()
    
    func jsonTask(with request: URLRequest, completionHandler completion: @escaping JSONTaskCompletionHandler) -> URLSessionDataTask {
        
        let task = session.dataTask(with: request) { (data, response, error) in
            
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.Error(.requestFailed))
                return
            }
            
            if httpResponse.statusCode == 200 {
               // print("data: ", data!)
                if let data = data {
                    do {
                        
                        let decoder = JSONDecoder()
                        decoder.keyDecodingStrategy = .convertFromSnakeCase
                        let responseModel = try decoder.decode(HalifaxBrandMapping.self, from: data)
                            DispatchQueue.main.async {
                                completion(.Success(responseModel))
                            }
                        
                    } catch {
                        completion(.Error(.jsonConversionFailure))
                    }
                } else {
                    completion(.Error(.invalidData))
                }
            } else {
                completion(.Error(.responseUnsuccessful))
                print("\(String(describing: error))")
            }
        }
        return task
    }
}

enum Result<T> {
    case Success(T)
    case Error(HalifaxApiError)
}

enum HalifaxApiError: Error {
    case requestFailed
    case jsonConversionFailure
    case invalidData
    case responseUnsuccessful
    case invalidURL
    case jsonParsingFailure
}
    

