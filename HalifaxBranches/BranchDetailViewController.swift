//
//  BranchDetailViewController.swift
//  HalifaxBranches
//
//  Created by Marijan Vukcevich on 3/6/19.
//

import UIKit

class BranchDetailViewController: UIViewController {

    
    @IBOutlet weak var branchName: UILabel!
    
    @IBOutlet weak var branchAddress: UILabel!
    
    @IBOutlet weak var branchPhone: UILabel!
    
    var branchesArray = [BranchInfo]()
    var index:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Branch Info"
       
        let cancelButton = UIButton()
        cancelButton.backgroundColor = .clear
        cancelButton.setTitleColor(UIColor.black, for: .normal)
        cancelButton.setTitle("Back", for: .normal)
        cancelButton.sizeToFit()
        cancelButton.addTarget(self, action: #selector(backBtnTapped), for: .touchUpInside)
        let cancelBarButton = UIBarButtonItem(customView: cancelButton)
        navigationItem.leftBarButtonItem = cancelBarButton
        
        getBranchInfo()
        
    }
    
    @objc func backBtnTapped() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func getBranchInfo() {
        
        for (i, value) in branchesArray.enumerated() {
            if (i == index) {
                branchName.text = value.branchName
                branchAddress.text = value.branchAddressLine! + ",\r\n" +
                    value.branchTownName! + " " +
                    value.branchPostCode! + "\r\n" +
                    value.branchCountry!
                branchPhone.text = value.branchPhone
            }
        }
    }
    
}
