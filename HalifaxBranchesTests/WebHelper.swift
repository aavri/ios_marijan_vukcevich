//
//  WebHelper.swift
//  HalifaxBranchesTests
//
//  Created by Marijan Vukcevich on 3/6/19.
//

import Foundation
import UIKit

public struct WebHelper {
    
    /**
     Add, update, or remove a query string parameter from the URL
     
     - parameter url:   the URL
     - parameter key:   the key of the query string parameter
     - parameter value: the value to replace the query string parameter, nil will remove item
     
     - returns: the URL with the mutated query string
     */
    public func addOrUpdateQueryStringParameter(url: String, key: String, value: String?) -> String {
        if let components = URLComponents(string: url) {
           
            var queryItems: [URLQueryItem] = (components.queryItems ?? [])
            
            for (index, item) in queryItems.enumerated() {
                // Match query string key and update
                if item.name.lowercased() == key.lowercased() {
                    if let v = value {
                        queryItems[index] = URLQueryItem(name: key, value: v)
                    } else {
                        queryItems.remove(at:index)
                    }
                    queryItems = (queryItems.count > 0 ? queryItems : nil)!
                    return components.string!
                }
            }
            
            // Key doesn't exist if reaches here
            if let v = value {
                // Add key to URL query string
                queryItems.append(URLQueryItem(name: key, value: v))
                return components.string!
            }
        }
        
        return url
    }
    
    /**
     Removes a query string parameter from the URL
     
     - parameter url:   the URL
     - parameter key:   the key of the query string parameter
     
     - returns: the URL with the mutated query string
     */
    public func removeQueryStringParameter(url: String, key: String) -> String {
        return addOrUpdateQueryStringParameter(url: url, key: key, value: nil)
    }
    
}
