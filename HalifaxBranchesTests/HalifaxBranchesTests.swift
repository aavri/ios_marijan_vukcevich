//
//  HalifaxBranchesTests.swift
//  HalifaxBranchesTests
//
//  Created by Marijan Vukcevich on 3/5/19.
//

import XCTest
@testable import HalifaxBranches

class HalifaxBranchesTests: XCTestCase {

    var webHelper: WebHelper!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        
        webHelper = WebHelper()
    }
    
    
    /*
    func testAddOrUpdateQueryStringParameter() {
        let value = "https://api.halifax.co.uk/open-banking/v2.2/branches?1234=SomeMockValue"
        
        let newValue = webHelper.addOrUpdateQueryStringParameter(url: value, key: "1234", value: "SomeMockValue")
        let expectedValue = "https://api.halifax.co.uk/open-banking/v2.2/branches"
    
    XCTAssertEqual(newValue, expectedValue,
    "String should be \(expectedValue)")
    }
    */
    
    func test_First_title() {
        // let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rt = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "goToFirstVC") as UIViewController
        let _ = rt.view
        XCTAssertEqual("Branches", rt.title)
    }
    
    func test_Second_title() {
       // let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rt = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "goToSecondVC") as UIViewController
        let _ = rt.view
        XCTAssertEqual("Branch Info", rt.title)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
